#sudo apt install python3-mysqldb
'''
req = "SELECT tt_news.uid, tt_news.datetime, tt_news.title, tt_news.image, tt_news.short, tt_news.bodytext, "
req += "tt_news.tx_rgmediaimages_config, tt_news.tx_rgmediaimages_width, tt_news.tx_rgmediaimages_height, "
req += "tt_news_cat_mm.uid_local, tt_news_cat_mm.uid_foreign, "
req += "tt_news_cat.uid, tt_news_cat.title "
req += "FROM tt_news "
req += "INNER JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_local = tt_news.uid "
req += "INNER JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign "
req += "WHERE tt_news.deleted = 0 AND tt_news.hidden = 0 "
'''

import MySQLdb, MySQLdb.cursors

db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="t3", cursorclass=MySQLdb.cursors.DictCursor)
cur = db.cursor()

def get_all_ttnews():
	req = "SELECT tt_news.uid, tt_news.datetime, tt_news.title, tt_news.image, tt_news.short, tt_news.bodytext, "
	req += "tt_news.tx_rgmediaimages_config, tt_news.tx_rgmediaimages_width, tt_news.tx_rgmediaimages_height "
	req += "FROM tt_news "
	req += "WHERE tt_news.deleted = 0 AND tt_news.hidden = 0 "
	req += "ORDER BY tt_news.datetime ASC "
	cur.execute( req )
	return cur.fetchall()

def get_ttnews_cats( news_id ):
	req = "SELECT tt_news_cat.uid, tt_news_cat.title "
	req += "FROM tt_news_cat "
	req += "INNER JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_foreign = tt_news_cat.uid "
	req += "WHERE tt_news_cat.hidden = 0 AND tt_news_cat.title != '' AND tt_news_cat_mm.uid_local = " + str( news_id )
	cur.execute( req )
	return cur.fetchall()

rows = get_all_ttnews()

print( len( rows ) )

for row in rows:
	print( row['title'] )
	if len( row['image'] ) > 0:
		imgs = row['image'].split(',')
		for i in imgs:
			print( '\t' + i )
	cats = get_ttnews_cats( row['uid'] )
	for cat in cats:
		print( '\t[' + str(cat['uid']) + ']:' + cat['title'] )

db.close()