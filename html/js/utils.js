function timestamp() {
	var now = new Date(); //.format("yyyymmdd h:MM:ss");
	var ts = now.getFullYear();
	var tmp;
	tmp = '' + now.getMonth();
	while( tmp.length < 2 ) { tmp = '0'+tmp; }
	ts += tmp;
	tmp = '' + now.getDate();
	while( tmp.length < 2 ) { tmp = '0'+tmp; }
	ts += tmp;
	tmp = '' + now.getHours();
	while( tmp.length < 2 ) { tmp = '0'+tmp; }
	ts += ' ' + tmp;
	tmp = '' + now.getMinutes();
	while( tmp.length < 2 ) { tmp = '0'+tmp; }
	ts += ':' + tmp;
	tmp = '' + now.getSeconds();
	while( tmp.length < 2 ) { tmp = '0'+tmp; }
	ts += ':' + tmp;
	return ts;
}

