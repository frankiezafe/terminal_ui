var form_login_enabled = false;
var form_search_enabled = false;
var form_prefs_enabled = false;
var current_user = 'public';
var req_history = 0;
var req_map = {}

function ui_hide_all( reset = true, login = false, search = false, prefs = false ) {
	if ( reset ) {
		form_login_enabled = login;
		form_search_enabled = search;
		form_prefs_enabled = prefs;
	}
	document.getElementById("form_login").style.display = "none";
	document.getElementById("form_search").style.display = "none";
	document.getElementById("form_prefs").style.display = "none";
}

function ui_show_login() {
	ui_hide_all( true, !form_login_enabled, false, false );
	if (form_login_enabled) { document.getElementById("form_login").style.display = "initial"; }
	return false;
}

function ui_show_search() {
	ui_hide_all( true, false, !form_search_enabled, false );
	if (form_search_enabled) { document.getElementById("form_search").style.display = "initial"; }
	return false;
}

function ui_show_pref() {
	ui_hide_all( true, false, false, !form_prefs_enabled );
	if (form_prefs_enabled) { document.getElementById("form_prefs").style.display = "initial"; }
	return false;
}

function ui_latest() {
	ui_hide_all();
	var req = {};
	req.title = 'latest';
	req.body = document.getElementById('tmp').innerHTML;
	display_request( req );
	return false;
}

function ui_list() {
	ui_hide_all();
	var req = {};
	req.title = 'list';
	req.body = document.getElementById('tmp').innerHTML;
	display_request( req );
	return false;
}

function ui_help() {
	ui_hide_all();
	var req = {};
	req.title = 'help';
	req.body = document.getElementById('ḧelp_content').innerHTML;
	display_request( req );
	return false;
}

function ui_repo() {
	ui_hide_all();
	var req = {};
	req.title = 'repositories';
	req.body = 'display of all repositories, sorted by date'; // document.getElementById('ḧelp_content').innerHTML;
	display_request( req );
	return false;
}

function filter_tag( tagname ) {
	ui_hide_all();
	var req = {};
	req.title = 'search tag['+tagname+']';
	req.body = '<br/>results for tag ' + tagname;
	display_request( req );
	return false;
}

function filter_db( dbname ) {
	ui_hide_all();
	var req = {};
	req.title = 'search db['+dbname+']';
	req.body = '<br/>results from database ' + dbname;
	display_request( req );
	return false;
}

function display_detail( title ) {
	ui_hide_all();
	var req = {};
	req.title = 'details <i>'+title+'</i>';
	req.body = document.getElementById('tmp_page').innerHTML;
	display_request( req );
	return false;
}

function display_request( req ) {
	
	if ( req.title in req_map ) {
		window.location.hash = '#req'+req_map[ req.title ];
		return;
	}
	/*
	if ( req_history == 0 ) {
		document.getElementById("data").innerHTML = '';
	}
	*/
	document.getElementById("data").innerHTML += '<a class="anchor" name="req'+req_history+'"></a><span class="user">'+current_user+'</span> '+timestamp()+' :: ' + req.title;
	document.getElementById("data").innerHTML += req.body;
	document.getElementById("data").innerHTML += '<hr/>';
	document.getElementById("ḧistoric").innerHTML += '<a href="#req'+req_history+'" title="'+req.title+'">'+req_history+'</a><br/>';
	window.location.hash = '#req'+req_history;
	req_map[ req.title ] = req_history;
	req_history++;
}

function ui_anim() {
	document.getElementById( "datetime_now" ).innerHTML = timestamp();
}

function ui_init() {
	ui_anim();
	window.setInterval(ui_anim, 1000);
}
